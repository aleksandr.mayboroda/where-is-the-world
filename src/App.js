import Header from './components/Header'
import Main from './components/Main'
import AppRoutes from './routes/AppRoutes'
import { SnackbarProvider } from 'notistack'
import { SnackbarUtilsConfigurator } from './utils/SnackBarUtils'

const App = () => {
  return (
    <SnackbarProvider>
      <SnackbarUtilsConfigurator />
      <Header />
      <Main>
        <AppRoutes />
      </Main>
    </SnackbarProvider>
  )
}

export default App
