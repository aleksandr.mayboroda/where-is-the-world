import {memo} from 'react'
import styled from 'styled-components'

const Button = styled.button`
  padding: 0 1rem;
  line-height: 2.5;
  text-transform: capitalize;
  border-radius: var(--radii);
  background-color: var(--colors-ui-base);
  color: var(--colors-text);
  box-shadow: var(--shadow);
  cursor: pointer;

  display: flex;
  align-items: center;
  gap: 0.75rem;

  border: none;
`

export default memo(Button)