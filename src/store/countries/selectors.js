const getCountries = () => state => state.countries.data
// const getOneCountry = (id) => state => state.countries.data.f
const getIsLoading = () => state => state.countries.isLoading

const def = {
  getCountries,
  getIsLoading,
}

export default def