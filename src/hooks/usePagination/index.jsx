import { useState, useEffect, useCallback } from 'react'
import styled from 'styled-components'

const Panel = styled.div`
    display: 'flex';
    align-items: 'center';
    /* justify-content: center; */
    padding: '10px';
    ${props => props?.styles && props.styles}
  `

  const Button = styled.button`
  outline: none;
  background-color: transparent;
  min-width: 43px;
  text-align: center;
  transition: 0.3s;
  font-weight: bolder;
  border: 1px solid #fff;
  border-radius: 7px;
  color: #fff;
  margin: 5px;
  padding: 10px;
  cursor: pointer;
  :hover {
    border-color: #000;
    color: #000;
  }
  ${props => props?.styles && props.styles}
`

const Arrow = styled.button`
  outline: none;
  background-color: transparent;
  min-width: 43px;
  text-align: center;
  transition: 0.3s;
  font-weight: bolder;
  border: 1px solid #fff;
  border-radius: 7px;
  color: #fff;
  margin: 5px;
  padding: 10px;
  cursor: pointer;
  text-transform: capitalize;
  :disabled {
    background-color: #959494e7;
    pointer-events: none;
    opacity: 0.6;
    cursor: inherit;
  }

  ${props => props?.styles && props.styles}
`

const usePagination = (dataArray = [], userSettings = null) => {

  const [perPage,setPerPage] = useState(4)

  const defaultSettings = {
    showButtonNumbers: true, // отображение номеров страниц
    showArrows: true, // отображать ли стрелки (prev, next)
    perPage: perPage, //количество элементов на странице
    dotsLeft: ' ...', //чтобы их можно было различать при нажатии
    dotsRight: '... ', //чтобы их можно было различать при нажатии
    arrows: {
      //объект с настройками стрелок
      showArrows: true,
      prevText: 'prev',
      nextText: 'next',
    },
    styles: {
      active: {
        backgroundColor: '#fff',color: '#000',borderColor: '#000'
      }
    }
  }

  const settings = userSettings
    ? { ...defaultSettings, ...userSettings }
    : defaultSettings

  // console.log('settings', settings)

  // const setPerPage = (newVal) => {
  //   settings.perPage = newVal
  // }

  const [defaultArray, setDefaultArray] = useState(dataArray)

  //нарезанный массив для отображения
  const [resultDataArray, setResultDataArray] = useState([])

  //текущая выбранная страница, по умолчанию 1
  const [activePage, setActivePage] = useState(1)

  //текущая выбранная кнопка, по умолчанию 1
  const [arrayOfCurrentButton, setArrayOfCurrentButton] = useState([])

  //Кол-во страниц всего - ???
  const pagesNumber = Math.floor(defaultArray.length / settings.perPage)

  //массив со страницами, которые нужно отображать
  const pageNumbers = []

  const memoSetPerPage = useCallback((newVal) => setPerPage(newVal),[])
  const memoSetDefaultArray = useCallback((newVal) => setDefaultArray(newVal),[])
  const memoSetActivePage = useCallback((newVal) => setActivePage(newVal),[])

  for (let i = 1; i <= pagesNumber; i++) {
    pageNumbers.push(i)
  }

  const btnClick = (number) => {
    if (number === settings.dotsLeft) {
      number = activePage - 2
    }
    if (number === settings.dotsRight) {
      number = activePage + 2
    }
    setActivePage(number)
  }

  useEffect(() => {
    const newPageNumbers = []

    if (activePage > 2) {
      newPageNumbers.push(1)
      if (activePage > 3) {
        newPageNumbers.push(settings.dotsLeft)
      }
    }

    let start = activePage - 1
    let end = activePage
    if (activePage > 1) {
      start = activePage - 2
    }
    if (activePage < pageNumbers.length) {
      end = activePage + 1
    }

    newPageNumbers.push(...pageNumbers.slice(start, end))

    if (activePage < pageNumbers.length - 1) {
      if (activePage < pageNumbers.length - 2) {
        newPageNumbers.push(settings.dotsRight)
      }
      newPageNumbers.push(pageNumbers.length)
    }

    setArrayOfCurrentButton(newPageNumbers)

    //индекс последнего и первого элементов на странице
    const lastElement = settings.perPage * activePage
    const firstElement = lastElement - settings.perPage
    setResultDataArray(defaultArray.slice(firstElement, lastElement))

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activePage, defaultArray])

  const returnButtonsPannel = useCallback(() => !arrayOfCurrentButton.length > 0 ? null : (
    <Panel styles={settings?.styles?.panel ? settings.styles.panel : null}>
      {settings.arrows.showArrows && (
        <Arrow
          styles={settings?.styles?.arrow ? settings.styles.arrow : null}
          disabled={activePage <= 1}
          onClick={() => setActivePage((prev) => (prev > 1 ? --prev : prev))}
        >
          {settings.arrows.prevText}
        </Arrow>
      )}
      {settings.showButtonNumbers &&
        arrayOfCurrentButton.map((number, index) => (
          <Button
            styles={settings?.styles?.button ? settings.styles.button : null}
            key={index}
            style={(number === activePage && settings?.styles?.active) ? settings.styles.active : {}}
            onClick={() => btnClick(number)}
          >
            {number}
          </Button>
        ))}

      {settings.arrows.showArrows && (
        <Arrow
          styles={settings?.styles?.arrow ? settings.styles.arrow : null}
          disabled={activePage >= pageNumbers.length}
          onClick={() =>
            setActivePage((prev) => (prev < pageNumbers.length ? ++prev : prev))
          }
        >
          {settings.arrows.nextText}
        </Arrow>
      )}
    </Panel>
  // eslint-disable-next-line react-hooks/exhaustive-deps
  ),[arrayOfCurrentButton, activePage])

  return {
    ButtonsPannel: returnButtonsPannel,
    resultArray: resultDataArray,
    setPerPage: memoSetPerPage,
    setDefaultArray: memoSetDefaultArray,
    setActivePage: memoSetActivePage,
  }
}

export default usePagination
