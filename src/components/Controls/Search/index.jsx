import {memo} from 'react'
import styled from 'styled-components'
import { IoSearch } from 'react-icons/io5'

import InputContainer from './InputContainer'

const Input = styled.input.attrs({
  type: 'search',
  placeholder: 'Search for country', 
})`
  margin-left: 2rem;
  border: none;
  outline: none;
  background-color: var(--colors-bg);
  color: var(--colors-text);
`

const Search = ({ search, setSearch }) => {
  return (
    <InputContainer>
      <IoSearch />
      <Input 
        value = {search}
        onChange = {ev => setSearch(ev.target.value)}
      />
    </InputContainer>
  )
}

export default memo(Search)