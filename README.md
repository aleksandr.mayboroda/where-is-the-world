Проект по уроку Михаила Непомнящего - Where is the world
https://www.youtube.com/watch?v=qVKGzyl0APQ&list=PLiZoB8JBsdzlRYLpRJyZgNnBNkQa7SU27&index=2

Описание:
Приложение с использоваением react-router-dom, которое отправляет запросы на сервис https://restcountries.com/ и отображает список стран с возможностью фильтрации по континенту или поиску по названию страны. Предусмотрен вариант дневной и ночной темы. Есть возможность перейти на страницу одной страны для просмотра информации, где присутствуют ссылки на соседние страны.

Ипользованы технологии
react
react-router-dom
react-redux (duck-style)
axios
react-select
react-icons
styled-components
notistack


Дополнительно сделано:

Написан хук для пагинации с возможностью стилизации, интегрирован в стили;
Добавлен react-redux;
Добавлены Snacks;
