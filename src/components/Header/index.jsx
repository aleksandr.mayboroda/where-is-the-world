import React, { useState, useEffect, memo } from 'react'
import Container from '../Container'
import { IoMoonOutline, IoMoon } from 'react-icons/io5'

import HeaderEl from './HeaderEl'
import Wrapper from './Wrapper'
import Title from './Title'
import ModeSwitch from './ModeSwitch'

const Header = () => {
  const [theme, setTheme] = useState('light')
  const toggleTheme = () => setTheme(theme === 'light' ? 'dark' : 'light')

  useEffect(() => {
    document.body.setAttribute('data-theme', theme)
  }, [theme])

  return (
    <HeaderEl>
      <Container>
        <Wrapper>
          <Title>Where is the world?</Title>
          <ModeSwitch onClick={toggleTheme}>
            {theme === 'light' ? (
              <IoMoonOutline size="14px" />
            ) : (
              <IoMoon size="14px" />
            )}{' '}
            <span style={{ marginLeft: '0.75rem' }}>{theme} theme</span>
          </ModeSwitch>
        </Wrapper>
      </Container>
    </HeaderEl>
  )
}

export default memo(Header)
