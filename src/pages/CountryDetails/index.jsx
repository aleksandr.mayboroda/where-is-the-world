import { useState, useEffect, memo } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { getCountryByName } from '../../api/countries'
import { IoArrowBack } from 'react-icons/io5'
import styled from 'styled-components'

import Button from '../../components/Button'
import CountryInfo from '../../components/CountryInfo'

const EmptyData = styled.h3`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: var(--fs-md);
  font-weight: var(--fw-bold);
`

const CountryDetails = () => {
  const { name } = useParams()
  const { push, goBack } = useHistory()
  const [data, setData] = useState(null)

  const getData = async () => {
    if (name) {
      const result = await getCountryByName(name)
      if (result) {
        setData(result)
      }
    }
  }

  useEffect(() => {
    getData()
    return () => setData(null)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [name])

  console.log('country', data)

  return (
  <div>
    <Button onClick={goBack}>
      <IoArrowBack />
      back
    </Button>
    {data && (<CountryInfo {...data} push={push} />)}
    {!data && (
      <EmptyData>No data is found by country name</EmptyData>
    )}
  </div>
  )
}

export default memo(CountryDetails)
