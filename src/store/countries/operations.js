import actions from './actions'
import {getAllCountries} from '../../api/countries'

const fetchCountries = () => async (dispatch) => {
  dispatch(actions.setIsLoading(true))
  const result = await getAllCountries()
  if(result)
  {
    dispatch(actions.setCountries(result))
  }
  dispatch(actions.setIsLoading(false))
}

const def = {
  fetchCountries,
}

export default def