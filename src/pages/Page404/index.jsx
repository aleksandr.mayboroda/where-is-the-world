import {memo} from 'react'

const Page404 = () => {
  return (
    <div>
      <h2>Page</h2>
      <h1>404</h1>
      <h4>This page doesn't exists</h4>
    </div>
  )
}

export default memo(Page404)
