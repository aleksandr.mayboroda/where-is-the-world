import { useEffect, memo, useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'

import Controls from '../../components/Controls'

import CountriesList from '../../components/CountriesList'
import CountryCard from '../../components/CountryCard'

import { useSelector, useDispatch } from 'react-redux'
import { countriesSelectors, countriesOperations } from '../../store/countries'

import usePagination from '../../hooks/usePagination'

const EmptyData = styled.h3`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: var(--fs-md);
  font-weight: var(--fw-bold);
`

const ContentWrapper = styled.div`
  padding: 1rem 0;
`

const styles = {
  // panel: `
  //   background-color: var(--colors-ui-base),
  // `,
  button: `
    color: var(--colors-text);
    border-radius: var(--radii);
    border: 1px solid transparent;
    box-shadow: var(--shadow);
    :hover {
      border-color: var(--colors-text);
    }
  `,
  arrow: `
    color: var(--colors-text);
    border-radius: var(--radii);
    border: 1px solid transparent;
    box-shadow: var(--shadow);
    :hover {
      border-color: var(--colors-text);
    }
    :disabled {
      background-color: transparent;
    }
  `,
  active: {
    borderColor: 'var(--colors-text)',
    color: 'var(--colors-text)'
  }
}

const Home = () => {
  // const [filteredCountries, setFilteredCountries] = useState([]) //надо ли...

  const dispatch = useDispatch()
  const countries = useSelector(countriesSelectors.getCountries())
  // const isLoading = useSelector(countriesSelectors.getIsLoading())

  const history = useHistory()

  const { ButtonsPannel, resultArray, setPerPage, setDefaultArray, setActivePage } = usePagination(
    // filteredCountries, //надо ли...
    countries,
    {
      // perPage: 8, //теперь передается отдельно
      styles,
    }
  )

  const getCountries = async () => {
    if (!countries.length) {
      dispatch(countriesOperations.fetchCountries())
    }
  }

  useEffect(() => {
    getCountries()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    handleSearch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countries])

  const handleSearch = useCallback((search, region, onPage) => {
    let data = [...countries]

    if (region) {
      setActivePage(1) //сбрасываем на 1ю страницу
      region = region.toLowerCase()
      data = data.filter((country) =>
        country.region.toLowerCase().includes(region)
      )
    }

    if (search) {
      setActivePage(1) //сбрасываем на 1ю страницу
      search = search.toLowerCase()
      data = data.filter((country) =>
        country.name.toLowerCase().includes(search)
      )
    }

    if (onPage) {
      setActivePage(1) //сбрасываем на 1ю страницу
      setPerPage(onPage)
    }

    // setFilteredCountries(data) //надо ли...
    setDefaultArray(data)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[countries])

  const countriesList = !resultArray.length
    ? []
    : resultArray.map((country) => ({
        key: country.name,
        name: country.name,
        img: country.flags.png,
        info: [
          {
            title: 'population',
            description: country.population.toLocaleString(),
          },
          { title: 'region', description: country.region },
          { title: 'capital', description: country.capital },
        ],
        onClick: () => history.push(`/country/${country.name}`),
      }))

  return (
    <>
      <Controls onSearch={handleSearch} />
      {countriesList.length > 0 && (
        <ContentWrapper>
          <ButtonsPannel />
          <CountriesList>
            {countriesList.map((count) => (
              <CountryCard {...count} />
            ))}
          </CountriesList>
          <ButtonsPannel />
        </ContentWrapper>
      )}

      {!countriesList.length && <EmptyData>No countries is found</EmptyData>}
    </>
  )
}

export default memo(Home)
