import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Home from '../pages/Home'
import CountryDetails from '../pages/CountryDetails'
import Page404 from '../pages/Page404'

const AppRoutes = ({countries, setCountries}) => {
  return (
    <Switch>
      <Route exact path="/">
        <Home countries={countries} setCountries={setCountries}/>
      </Route>
      <Route exact path="/country/:name">
        <CountryDetails />
      </Route>
      <Route path="*">
        <Page404 />
      </Route>
    </Switch>
  )
}

export default AppRoutes
