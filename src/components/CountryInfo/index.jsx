import { useState, useEffect, memo } from 'react'
import styled from 'styled-components'
import { getCoutriesByCodes } from '../../api/countries'

import { snackActions } from '../../utils/SnackBarUtils';

const Wrapper = styled.section`
  margin-top: 3rem;
  width: 100%;
  display: grid;
  grid-template-columns: 100%;
  gap: 2rem;

  @media (min-width: 767px) {
    grid-template-columns: minmax(100px, 400px) 1fr;
    align-items: center;
    gap: 5rem;
  }

  @media (min-width: 1024px) {
    grid-template-columns: minmax(400px, 600px) 1fr;
  }
`

const CountryImage = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: contain;
`

const CountryTitle = styled.h1`
  margin: 0;
  font-weight: var(--fw-normal);
`
const ListGroup = styled.div`
  display: flex;
  flex-direction: column;
  gap: 2rem;

  @media (min-width: 1024px) {
    gap: 4rem;
    flex-direction: row;
  }
`
const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`
const ListItem = styled.li`
  line-height: 1.8;

  & > b {
    font-weight: var(--fw-bold);
    text-transform: capitalize;
  }
`

const Meta = styled.div`
  margin-top: 3rem;
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
  align-items: flex-start;

  & > b {
    font-weight: var(--fw-bold);
    text-transform: capitalize;
    min-width: 135px;
  }

  @media (min-width: 767px) {
    flex-direction: row;
    align-items: center;
  }
`

const TagGroup = styled.div`
  display: flex;
  gap: 1rem;
  flex-wrap: wrap;
`

const Tag = styled.span`
  padding: 0 1rem;
  line-height: 1.5;
  cursor: pointer;
  background-color: var(--colors-ui-base);
  box-shadow: var(--shadow);
`

const CountryInfo = (props) => {
  const {
    name,
    capital,
    flag,
    population,
    region,
    nativeName,
    subregion,
    topLevelDomain,
    currencies = [],
    languages = [],
    borders = [],
    push, //это чтобы не создавать объект каждый раз в этом компоненте
  } = props

  const [borderCountries, setBorderCountries] = useState([])
  const getSiblingsCountries = async () => {
    const result = await getCoutriesByCodes(
      borders.map((bor) => bor.toLowerCase()).join(',')
    )
    console.log('res', result)
    if (result) {
      setBorderCountries(result.map((country) => country.name))
      snackActions.success('Sibling countries is loaded successfully')
    }

    //До этого не доходит, если ошибка в запросе
    // else
    // {
    //   snackActions.error('Trouble happened')
    // }
  }

  useEffect(() => {
    if (borders.length > 0) {
      getSiblingsCountries()
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [borders])

  const lists = [
    [
      {
        title: 'native name',
        description: nativeName,
      },
      {
        title: 'population',
        description: population.toLocaleString(),
      },
      {
        title: 'region',
        description: region,
      },
      {
        title: 'sub region',
        description: subregion,
      },
      {
        title: 'capital',
        description: capital,
      },
    ],
    [
      {
        title: 'top level domain',
        description: topLevelDomain,
      },
      {
        title: 'currencies',
        description: currencies?.map((curr) => curr.name).join(', '),
      },
      {
        title: 'languages',
        description: languages?.map((lang) => lang.name).join(', '),
      },
    ],
  ]

  return (
    <Wrapper>
      <CountryImage src={flag} alt={name} />
      <div>
        <CountryTitle>{name}</CountryTitle>
        <ListGroup>
          {lists.map((list, index) => (
            <List key={index}>
              {list.map((item) => (
                <ListItem key={item.title}>
                  <b>{item.title}</b>: {item.description}
                </ListItem>
              ))}
            </List>
          ))}
        </ListGroup>
        <Meta>
          <b>border countries:</b>
          {!borderCountries.length && <span>There is no border countries</span>}
          {borderCountries.length > 0 && (
            <TagGroup>
              {borderCountries.map((countr) => (
                <Tag key={countr} onClick={() => push(`/country/${countr}`)}>
                  {countr}
                </Tag>
              ))}
            </TagGroup>
          )}
        </Meta>
      </div>
    </Wrapper>
  )
}

export default memo(CountryInfo)
