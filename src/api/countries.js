// import axios from 'axios'
// import {FetchData} from './base'
import {FetchData} from './base'

const BASE_URL = 'https://restcountries.com/v2/'
const api = new FetchData(BASE_URL)

export const getAllCountries = () =>
  api.get('all', {fields: 'name,capital,flags,population,region'})
  // api.get('all', {fields: 'name,capital,flags,population,region,nativeName,subregion,topLevelDomain,currencies,languages,borders'})
  // api.get('all')
    .then(response => response.data)

export const getCountryByName = (name) => 
  api.get(`name/${name}`, {fullText: true})
    .then(response => response.data[0])

export const getCoutriesByCodes = (codes) => api.get('alpha', {codes}).then(response => response.data)