import types from './types'

const setCountries = array => ({
  type: types.SET_COUNTRIES,
  payload: array,
})

const setIsLoading = value => ({
  type: types.SET_IS_LOADING,
  payload: value,
})

const def = {
  setCountries,
  setIsLoading,
}

export default def