import types from './types'

const initialState = {
  data: [],
  isLoading: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_COUNTRIES: {
      return {...state, data: action.payload}
    }
    case types.SET_IS_LOADING: {
      return {...state, isLoading: action.payload}
    }
    default:
      return state
  }
}

export default reducer
