import React, { useState, useEffect, memo, useCallback } from 'react'

import Wrapper from './Wrapper'
import Search from './Search'
import CustomSelect from './CustomSelect'

const regionOptions = [
  { value: 'Africa', label: 'Africa' },
  { value: 'America', label: 'America' },
  { value: 'Asia', label: 'Asia' },
  { value: 'Europe', label: 'Europe' },
  { value: 'Oceania', label: 'Oceania' },
]

const perPageOptions = [
  { value: 10, label: 10 },
  { value: 20, label: 20 },
  { value: 50, label: 50 },
]

const Controls = ({ onSearch }) => {
  const [search, setSearch] = useState('')
  const [region, setRegion] = useState('')
  const [perPage, setPerPage] = useState(perPageOptions[0])

  const memoSetSearch = useCallback((newSearch) => setSearch(newSearch),[])
  const memoSetRegion = useCallback((newRegion) => setRegion(newRegion),[])
  const memoSetPerPage = useCallback((newPerPage) => setPerPage(newPerPage),[])

  useEffect(() => {
    const regionValue = region?.value || ''
    const onPage = perPage?.value || ''
    onSearch(search, regionValue, onPage)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search, region, perPage])

  return (
    <Wrapper>
      <Search search={search} setSearch={memoSetSearch} />
      <CustomSelect
        options={regionOptions}
        placeholder={'Filter by region'}
        isClearable={true}
        isSearchable={false}
        value={region}
        onChange={memoSetRegion}
      />
      <CustomSelect
        options={perPageOptions}
        placeholder={'Countries per page'}
        value={perPage}
        onChange={memoSetPerPage}
      />
    </Wrapper>
  )
}

export default memo(Controls)
