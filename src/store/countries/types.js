const SET_COUNTRIES = 'countries_flags/countries/SET_COUNTRIES'
const SET_IS_LOADING = 'countries_flags/countries/SET_IS_LOADING'

const def = {
  SET_COUNTRIES,
  SET_IS_LOADING,
}

export default def