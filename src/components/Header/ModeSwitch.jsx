import styled from 'styled-components'

const ModeSwitch = styled.div`
color: var(--colors-text);
font-size: var(--fs-sm);
font-weight: var(--fw-bold);
cursor: pointer;
text-transform: capitalize;
`

export default ModeSwitch